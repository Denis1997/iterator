/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Mainprg {
    public static void main(String[] args)
    {
        Basket basket = new Basket();
        Fruit apple = new Fruit("apple");
        Fruit banana = new Fruit("banana");
        Fruit orange = new Fruit("orange");
        
        basket.addFruit(apple);
        basket.addFruit(banana);
        basket.addFruit(orange);
        basket.addFruit(apple);
        basket.addFruit(banana);
        basket.addFruit(apple);
        while(basket.hasNext())
            System.out.println(basket.next().getName());
    }
}
