
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Basket implements Iterator<Fruit>{
    int iterPos = 0;
    int iterCount = -1;
    List <Fruit> fruits = new ArrayList<>();

    public void addFruit(Fruit f)
    {
        fruits.add(f);
    }
    
    private void incIter()
    {
        if (iterPos < fruits.size() / 2)
        {
            iterPos = fruits.size() - iterPos - 1;
        }
        else
        {
            iterPos = fruits.size() - iterPos;
        }
            
        iterCount++;
        if (iterCount == fruits.size())
        {
            iterPos = 0;
            iterCount = -1;
        }
    }
    
    @Override
    public boolean hasNext() {
        if (iterCount + 1 == fruits.size())
            return false;
        return true;
    }

    @Override
    public Fruit next() {
        Fruit ret = fruits.get(iterPos);
        incIter();
        return ret;
    }
}
